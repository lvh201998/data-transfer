

import com.fasterxml.jackson.databind.ObjectMapper;
import dto.req.ITOrderDTOCreate;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.json.JSONObject;

import java.io.IOException;

public class t2 {


    public static void main(String[] args) throws IOException {
        String data = "{\n" +
                "  \"accessionNumber\": \"22.0206.004051\",\n" +
                "  \"clinicalDiagnosis\": \"Polyp đại tràng- Chưa loại trừ TBMMN\",\n" +
                "  \"ignoreAccessionNumberDuplicates\": true,\n" +
                "  \"orderDatetime\": \"20220419170000\",\n" +
                "  \"orderNumber\": \"22.0206.004051\",\n" +
                "  \"patient\": {\n" +
                "    \"pid\": \"15011703\",\n" +
                "    \"fullname\": \"ĐINH VĂN HẠNH\",\n" +
                "    \"gender\": \"M\",\n" +
                "    \"birthDate\": \"19560101\",\n" +
                "    \"address\": \"241 Trần Nguyên Hãn, Phường Trần Nguyên Hãn, Quận Lê Chân, Hải Phòng\"\n" +
                "  },\n" +
                "  \"instructions\": \"\",\n" +
                "  \"requestedDepartmentCode\": \"\",\n" +
                "  \"requestedDepartmentName\": \"Khoa Ngoại Tổng Hợp\",\n" +
                "  \"referringPhysicianCode\": \"\",\n" +
                "  \"referringPhysicianName\": \"Vũ Văn Huy\",\n" +
                "  \"services\": [\n" +
                "    {\n" +
                "      \"requestedDatetime\": \"20220419170000\",\n" +
                "      \"requestedModalityType\": \"DX\",\n" +
                "      \"requestedNumber\": \"123.255562890759449.1730531709486465\",\n" +
                "      \"requestedProcedureCode\": \"MRI1329\",\n" +
                "      \"requestedProcedureName\": \"Chụp cộng hưởng từ sọ não có tiêm chất tương phản (0.2-1.5T)\"\n" +
                "    }\n" +
                "  ]\n" +
                "}";

        ITOrderDTOCreate dataReq= new ObjectMapper().readValue(data, ITOrderDTOCreate.class);

        callApi(dataReq);

    }

    private static void callApi(ITOrderDTOCreate data) throws IOException {
        JSONObject obj = new JSONObject(data);
        StringEntity body= new StringEntity(obj.toString(), "UTF-8");
        HttpPost http = new HttpPost("http://localhost:6789/conn/ws/rest/v1/hospital/31313-002/order");
        HttpClient client = HttpClients.createDefault();
        http.addHeader("Authorization", "Basic Y29ubjMxMzEzLTAwMjppVGVjaDEyMzQ=");
        http.setHeader("Content-Type", "application/json;charset=UTF-8");
        http.setEntity(body);
        HttpResponse httpResponse = client.execute(http);
        System.out.println(httpResponse.toString());

    }

}
