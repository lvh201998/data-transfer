import dto.req.ITOrderDTOCreate;
import dto.req.ITOrderProcedureDTO;
import dto.req.ITPatientDTO;
import dto.res.DbDataRes;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.sql.*;
import java.util.*;
import java.util.Date;

import static dto.constant.DateUtil.formatDate;

public class TestData {
    static String url = "jdbc:mariadb://192.168.1.32:43306/ris";
    static String use = "root";
    static String pass = "haiphong2021";

    static int count1 = 0;
    static int count2 = 0;
    static int countE = 0;
    static List<Integer> idError = new ArrayList();

    static String sql1 = "select\n" +
            "    fm.id,\n" +
            "    fm.patientFK,\n" +
            "    fm.accessionNumber,\n" +
            "    fm.groupModality,\n" +
            "    fm.diagnosis,\n" +
            "    fm.jsonData,\n" +
            "    rp.address,\n" +
            "    rp.pid,\n" +
            "    rp.birthYear,\n" +
            "    rp.name,\n" +
            "    rp.sex,\n" +
            "    rp.phone,\n" +
            "    fm.studyIUID\n" +
            "from ris_mwl fm\n" +
            "left join ris_patient rp on fm.patientFK = rp.id\n" +
            "limit 30000 offset {{offsetValue}}";

    static String sql2 = "select\n" +
            "    rp.address,\n" +
            "    rp.pid,\n" +
            "    rp.birthYear,\n" +
            "    rp.name,\n" +
            "    rp.sex,\n" +
            "    rp.phone\n" +
            "from ris_patient rp\n" +
            "where rp.id = {{id}}";

    private static Map<String, String> modality = new HashMap<String, String>() {
        {
            put("1", "CT");
            put("2", "MR");
            put("11", "DX");
            put("9", "US");
            put("17", "ES");
            put("19", "OP");
            put("18", "SC");
            put("16", "ECG");
            put("10", "ED");
            put("12", "XA");
        }
    };

    public static void main(String[] args) throws IOException {
        Long start = new Date().getTime();
        for(int i = 0; i< 30 ; i++){
            getAndCallApi(i*30000);
        }
        Long end = new Date().getTime();
        System.out.println("time :" + (end -start));
    }

    private static void getAndCallApi(int offset) {
        sql1 = sql1.replace("{{offsetValue}}", offset+ "");
        Long s = new Date().getTime();
        System.out.println("------start------");
        List<DbDataRes> list = new ArrayList<>();
        try (Connection conn = DriverManager.getConnection(url, use, pass)) {
            try (Statement stmt = conn.createStatement()) {
                try (ResultSet rs = stmt.executeQuery(sql1)) {
                    while (rs.next()) {
                        DbDataRes res;
                        res = mapToRes(rs);
                        sql2 = sql2.replace("{{id}}", res.getPatientFK() + "");
                        try (ResultSet rs1 = stmt.executeQuery(sql2)) {
                            while (rs1.next()) {
                               mapToResPatient(rs1, res);
                            }
                        }
                        list.add(res);
                        count1++;
                        System.out.println(count1);
                    }
                }


            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        Long s2 = new Date().getTime();
        System.out.println("------done getdataa -----" + (s2 - s));

        List<ITOrderDTOCreate> list1 = new ArrayList<>();

        list.forEach(data -> {
            ITOrderDTOCreate da = convertToReq(data);
            if (da != null)
                list1.add(da);
            else {
                idError.add(data.getId());
//                System.out.println(idError.toString());
                countE ++;
//                System.out.println("id: " +data.getId() + " error: " + new JSONObject(data));
            }
        });

        list1.forEach(data -> {
            try {
                callApi(data);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        Long s3 = new Date().getTime();
        System.out.println("done: " + (s3 - s));


        System.out.println(count1);
        System.out.println(countE);
        System.out.println(count2);
        System.out.println(idError);
        System.out.println(idError.toString().split(",").length);
    }

    private static void callApi(ITOrderDTOCreate data) throws IOException {
        count2++;
        JSONObject obj = new JSONObject(data);
//        System.out.println(obj);
        StringEntity body = new StringEntity(obj.toString(), "UTF-8");
        HttpPost http = new HttpPost("http://localhost:6789/conn/ws/rest/v1/hospital/31313/order");
        HttpClient client = HttpClients.createDefault();
        http.addHeader("Authorization", "Basic Y29ubjMxMzEzLTAwMjppVGVjaDEyMzQ=");
        http.setHeader("Content-Type", "application/json;charset=UTF-8");
        http.setEntity(body);
        HttpResponse httpResponse = client.execute(http);

        System.out.println("call api: " +count2);

    }

    private static ITOrderDTOCreate convertToReq(DbDataRes data) {
        Map<String, String> jsonData = getDataInJson(data.getJsonData());
        ITOrderDTOCreate object = new ITOrderDTOCreate();
        if (jsonData == null) {
            return null;
        }
        object.setOrderNumber(data.getAccessionNumber());  //
        object.setModalityType(data.getModalityCode());  //loại chụp
        object.setAccessionNumber(data.getAccessionNumber()); //
        object.setClinicalDiagnosis(data.getDiagnosis());    //chuan doan
        object.setInstructions(jsonData.get("comment"));       //ghi chú
        object.setRequestedDepartmentCode(""); //mã khoa chỉ định
        object.setRequestedDepartmentName(jsonData.get("department"));   //ten khoa chỉ định
        object.setReferringPhysicianCode("");    // mã bác sĩ chỉ định
        object.setReferringPhysicianName(jsonData.get("doctor"));  //tên bách sĩ chỉ định
        object.setOrderDatetime(jsonData.get("orderDate"));     // ngày giờ chỉ định
//
        ITPatientDTO patient = new ITPatientDTO();
        patient.setAddress(data.getAddress());   //địa chỉ
        String birthDay = data.getBirthYear() != null & !data.getBirthYear().isEmpty() ? data.getBirthYear().concat("0101") : "";
        patient.setBirthDate(birthDay); // ngày tháng năm sinh
        patient.setFullname(data.getName());   //tên
        patient.setGender(data.getSex());
        patient.setPid(data.getPid());  //id bệnh nhân

        patient.setPhone(data.getPhone());
        object.setPatient(patient);

        ITOrderProcedureDTO service = new ITOrderProcedureDTO();
        service.setRequestedNumber(data.getStudyIUID());  //mã uuid lay từ ca khám
        service.setRequestedProcedureCode(jsonData.get("code"));    //mã số chỉ định chụp his
        service.setRequestedProcedureName(jsonData.get("name"));   // mô tả
        service.setRequestedModalityType(jsonData.get("group"));    //loại chụp
        service.setRequestedDatetime(jsonData.get("orderDate"));    // ngày giờ chỉ định

        object.setServices(Collections.singletonList(service));
        return object;
    }

    private static Map<String, String> getDataInJson(String data) {
        Map<String, String> map = new HashMap<>();
        if (data == null) {
            return null;
        }
        String dataNew = data;
        if(data.contains("\\\"")) {
            dataNew = dataNew.substring(1, data.length() -1);
            dataNew = dataNew.replace("\\", "");
        }
        JSONObject obj = null;
        try {
            obj = new JSONObject(dataNew);
        } catch (Exception ex) {
//            System.out.println("error");
//            System.out.println(data);

            System.out.println("convert json data error");
        }
        String comment = obj.getString("comment");
        map.put("comment", comment);

        JSONObject clinical = obj.getJSONObject("clinical");

        try {
            String department = clinical.getString("department");
            map.put("department", department);

            String doctor = clinical.getString("doctor");
            map.put("doctor", doctor);
        } catch (Exception e) {
            System.out.println("department -doctor - error");
        }

        String orderDate = obj.getString("orderDate");
        map.put("orderDate", formatDate(orderDate));


        JSONArray orderss = obj.getJSONArray("orders");
        if (!orderss.isEmpty()) {
            JSONObject orders = (JSONObject) orderss.get(0);
            String code = orders.getString("code");
            map.put("code", code);
            String name = orders.getString("name");
            map.put("name", name);
            String group = orders.getString("group");
            map.put("group", group);
        } else {
            return null;
        }

        return map;
    }

    private static DbDataRes mapToRes(ResultSet rs) throws SQLException {
        DbDataRes res = new DbDataRes();
        res.setId(rs.getInt("id"));
        res.setPatientFK(rs.getInt("patientFK"));
        res.setAccessionNumber(rs.getString("accessionNumber"));
        res.setModalityCode(modality.get(rs.getString("groupModality")));
        res.setDiagnosis(rs.getString("diagnosis"));
        res.setJsonData(rs.getString("jsonData"));
        res.setStudyIUID(rs.getString("studyIUID"));

        return res;
    }

    private static DbDataRes mapToResPatient(ResultSet rs, DbDataRes res) throws SQLException {
        res.setAddress(rs.getString("address"));
        res.setPid(rs.getString("pid"));
        res.setBirthYear(rs.getString("birthYear"));
        res.setName(rs.getString("name"));
        res.setSex(rs.getString("sex"));
        res.setPhone(rs.getString("phone"));

        return res;
    }
}
