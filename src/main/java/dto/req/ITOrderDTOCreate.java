package dto.req;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class ITOrderDTOCreate {

    private String accessionNumber; //

    private String admissionType;

    private String clinicalDiagnosis;

    private String encounterNumber;

    private boolean ignoreAccessionNumberDuplicates = true;

    private String modalityType;

    private String orderDatetime;

    private String orderNumber; //

    private ITPatientDTO patient;//

    private String instructions;

    private String requestedDepartmentCode;

    private String requestedDepartmentName;

    private String referringPhysicianCode;

    private String referringPhysicianName;

    private String priority;


    private List<ITOrderProcedureDTO> services;//
}

