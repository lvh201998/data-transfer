package dto.req;

import lombok.Data;

@Data
public class ITPatientDTO {

    private String id;

    private String initialSecret;

    private String pid;

    private String fullname;

    private String gender;

    private String birthDate;

    private String phone;

    private String email;

    private String address;

}
