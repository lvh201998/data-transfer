package dto.req;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
public class ITOrderProcedureDTO  {

    private String icdCode;

    private String requestedDatetime;

    private String requestedModalityType;

    private String requestedNumber;

    private String requestedProcedureCode;

    private String requestedProcedureName;
}

