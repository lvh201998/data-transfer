package dto.constant;

public enum VRWorklistRequestStatus {
    NEW,
    CANCELLED
}
