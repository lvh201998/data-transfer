package dto.constant;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {

    public static final DateFormat VR_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public static final DateFormat HIS_DATE_FORMAT = new SimpleDateFormat("yyyyMMddHHmmss");

    public static String formatDate(String date) {
        try {
            Date newDate = DateUtil.VR_DATE_FORMAT.parse(date);
            return DateUtil.HIS_DATE_FORMAT.format(newDate);
        } catch (ParseException ex) {
            return date;
        }
    }
}
