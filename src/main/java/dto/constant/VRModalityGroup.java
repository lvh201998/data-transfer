package dto.constant;

public enum VRModalityGroup {
    CT, MR, US, ED, DR, DX, CR, FE, TDCN
}
