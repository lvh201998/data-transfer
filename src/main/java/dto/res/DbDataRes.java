package dto.res;

import lombok.Data;

@Data
public class DbDataRes {
    private int id;

    private int patientFK;
    private String accessionNumber;
    private String modalityCode;
    private String diagnosis;
    private String jsonData;
    private String address;
    private String pid;
    private String birthYear;
    private String name;
    private String sex;
    private String phone;
    private String studyIUID;

}
